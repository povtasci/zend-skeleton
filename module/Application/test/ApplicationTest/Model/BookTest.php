<?php
namespace ApplicationTest\Model;

use Application\Model\Book;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class BookTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__ . '/../../../../../config/application.config.php'
        );
        parent::setUp();
    }

    public function testInitialValues()
    {
        $book = new Book();

        $this->assertNull(
            $book->getId(),
            '"book_id" should initially be null'
        );

        $this->assertNull(
            $book->getTitle(),
            '"book_title" should initially be null'
        );

        $this->assertNull(
            $book->getDescription(),
            '"book_description" should initially be null'
        );

        $this->assertNull(
            $book->getAuthorId(),
            '"book_author_id" should initially be null'
        );

        $this->assertNull(
            $book->getImage(),
            '"book_image" should initially be null'
        );

        $this->assertNull(
            $book->getPdf(),
            '"book_pdf" should initially be null'
        );

        $this->assertNull(
            $book->getCreatedAt(),
            '"book_created_at" should initially be null'
        );

        $this->assertSame(
            'pending',
            $book->getStatus(),
            '"book_status" should initially be "pending"'
        );
    }

    public function testGetValues()
    {
        $args = array(
            "book_id" => 1,
            "book_title" => "Test",
            "book_description" => "Test Description",
            "book_author_id" => 2,
            "book_image" => "/pics/img.jpg",
            "book_pdf" => null,
            "book_status" => 'active',
        );
        $book = new Book($args);

        $this->assertSame(
            $args['book_id'],
            $book->getId()
        );
        $this->assertSame(
            $args['book_title'],
            $book->getTitle()
        );
        $this->assertSame(
            $args['book_description'],
            $book->getDescription()
        );
        $this->assertSame(
            $args['book_author_id'],
            $book->getAuthorId()
        );
        $this->assertSame(
            $args['book_image'],
            $book->getImage()
        );
        $this->assertSame(
            $args['book_pdf'],
            $book->getPdf()
        );
        $this->assertSame(
            $args['book_status'],
            $book->getStatus()
        );
    }
}