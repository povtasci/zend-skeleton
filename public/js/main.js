var getNextPage = function(page, el) {
    $(".loader").removeClass("hide");
    $(el).addClass("hide");
    $.post("/action/get-books", {page: page, genre: $(el).data('genre'), author: $(el).data('author')}, function(res) {
        if (res.returnCode == 101) {
            $(el).closest(".seeMoreBlock").before(res.result.html);
            $(el).closest(".seeMoreBlock").remove();
        } else {
            alert(res.message);
        }
    }, 'json');
}