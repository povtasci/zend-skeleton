$(document).ready(function () {
    $(".ajaxForm").ajaxForm({
        beforeSubmit: function () {
        },
        success: function (res, status, xhr, $form) {
            if (res.returnCode == 101) {
                showSuccessAlert(res.msg);
                if (res.result && res.result.redirectTo) {
                    window.location.href = res.result.redirectTo;
                }
            } else {
                var error_msg = "";
                $.each(res.msg, function (index, value) {
                    $.each(value, function (i, v) {
                        console.log(error_msg);
                        if (error_msg.search(v) == -1) {
                            if (error_msg) {
                                error_msg = error_msg + ",<br>" + v;
                            } else {
                                if (typeof v == 'string')
                                    error_msg = v;
                            }
                        }
                    });
                });
                showDangerAlert(error_msg);
            }
        }
    });

    $('.imageUploadInput').change(function () {
        var form = $(this).closest(".imageUploadForm");
        var type = $(form).find("input[name='type']").val();
        $(form).ajaxForm({
            success: function (res) {
                if (res.returnCode == 101) {
                    if ($(form).data("type") == "image") {
                        $(".imagesStack").html(imageBoxInStackTemplate.replace("__url__", res.result.url).replace('/__id__/g', res.result.imageId));
                        $("input[name='book_image']").val(res.result.url);
                    } else {
                        $(".pdfStack").html(pdfBoxInStackTemplate.replace("__url__", res.result.url).replace('/__id__/g', res.result.imageId));
                        $("input[name='book_pdf']").val(res.result.url);
                    }
                    $(form).closest(".modal").modal('hide');
                } else {
                    showDangerAlert(res.msg);
                }
            }
        }).submit();
    });
});

var imageBoxInStackTemplate = "" +
    '<div class="imageBox smart-form">' +
    '<section>' +
    '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="removeImage(this)">' +
    '<i class="fa fa-times-circle"></i>' +
    ' Remove' +
    '</a>' +
    '</section>' +
    '<img src="__url__">' +
    '</div>';

var pdfBoxInStackTemplate = "" +
    '<div class="imageBox smart-form">' +
    '<section>' +
    '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="removePdf(this)">' +
    '<i class="fa fa-times-circle"></i>' +
    ' Remove' +
    '</a>' +
    '</section>' +
    '<a href="__url__" target="_blank">view pdf</a>' +
    '</div>';

function showDangerAlert(content) {
    $.bigBox({
        title: "Alert",
        content: content,
        color: "#C46A69",
        icon: "fa fa-warning shake animated",
        timeout: 5000,
        sound: false

    });
}

function showSuccessAlert(content) {
    $.bigBox({
        title: "Message",
        content: content,
        color: "#739E73",
        timeout: 5000,
        icon: "fa fa-check",
        sound: false
    });
}

function removeImage(el) {
    if (confirm("Are you sure ?")) {
        $(el).closest(".imagesStack").html('');
        $("input[name='book_image']").val('');
    }

    return false;
}

function removePdf(el) {
    if (confirm("Are you sure ?")) {
        $(el).closest(".pdfStack").html('');
        $("input[name='book_pdf']").val('');
    }

    return false;
}

function deleteBook(el, id) {
    if (confirm("Are you sure ?")) {
        $.post("/admin-action/delete-book/" + id, function (res) {
            if (res.returnCode == 101) {
                $(el).closest("tr").remove();
                showSuccessAlert(res.msg);
            } else {
                showDangerAlert(res.msg);
            }
        }, "json");
    }

    return false;
}

function deleteAuthor(el, id) {
    if (confirm("Are you sure ?")) {
        $.post("/admin-action/delete-author/" + id, function (res) {
            if (res.returnCode == 101) {
                $(el).closest("tr").remove();
                showSuccessAlert(res.msg);
            } else {
                showDangerAlert(res.msg);
            }
        }, "json");
    }

    return false;
}

function deleteGenre(el, id) {
    if (confirm("Are you sure ?")) {
        $.post("/admin-action/delete-genre/" + id, function (res) {
            if (res.returnCode == 101) {
                $(el).closest("tr").remove();
                showSuccessAlert(res.msg);
            } else {
                showDangerAlert(res.msg);
            }
        }, "json");
    }

    return false;
}